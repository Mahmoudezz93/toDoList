// import Navigation liberaries //
import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

// import pages //

// Welcome Page // 
import welcomePage from '../Screens/WelcomePage';

// Auth Stack // 
import loginPage from '../Screens/Auth/login';
import SignUpPage from '../Screens/Auth/signup';

// Drawer Stack // 
import SideBar from '../Screens/Drawer/index';

// Main Stack // 
import HomePage from '../Screens/main/HomePage';
import MyProfile from '../Screens/main/MyProfile';

// .............. // 




const MainStackNavigator = createStackNavigator();
export const MainNavigator = ()=> 
{
  return (
    <MainStackNavigator.Navigator initialRouteName="Home" headerMode="none">
        <MainStackNavigator.Screen name="Home" component={HomePage} />
        <MainStackNavigator.Screen name="MyProfile" component={MyProfile} />
    </MainStackNavigator.Navigator>
)
}


const DrawerStackNavigator = createDrawerNavigator();
export const  DrawerNavigator = () =>
{
  return  (
    <DrawerStackNavigator.Navigator 
        initialRouteName={'Home'}
        
        drawerContentOptions={{  activeTintColor: "#e91e63"}}
        drawerPosition={ "right"}
        drawerContent={(props) => <SideBar {...props} />} >
    <DrawerStackNavigator.Screen name="Home" component={MainNavigator} />
    </DrawerStackNavigator.Navigator>
    )
}

const LoginStack = createStackNavigator();
// This is the main container for the application that should contain all other stack 
// the application                  
export default AppContainer= props => {
    return (
    <NavigationContainer >
        <LoginStack.Navigator headerMode="false" initialRouteName='Welcome' >
            <LoginStack.Screen name="Welcome" component={welcomePage} />
            <LoginStack.Screen name="login" component={loginPage} />
            <LoginStack.Screen name="SignUp" component={SignUpPage} />
            <LoginStack.Screen name="Drawer" component={DrawerNavigator}  />

        </LoginStack.Navigator>
    </NavigationContainer>
    );
};

