import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

// React Navigation V6 
import AppNavigator from"../toDoList/Navigation/AppContainer";
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
// const AppContainer = createNativeStackNavigator();

export default function App() {
  return (
    <AppContainer/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
